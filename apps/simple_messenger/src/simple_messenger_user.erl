%% @doc 
%% This module contains user side API to start client and communicate with other users.
%% @end

-module(simple_messenger_user).
-export([login/1, logout/0, send/2, who/0, yell/1]).
-include("server_config.hrl").


-spec login(string()) -> any().
%% @doc Login user to server.
%% This function also checks if the client is already running and if
%% username is unique (on server side).
%% @end
login(UserName) ->
	case whereis(client) of
		undefined -> 
			register(client,
					 spawn_link(simple_messenger_client, loginClient, [?NODE_SERVER, UserName]));
		_ -> 
			client_already_logged
	end.
-spec logout() -> any().
%% @doc Logout user from server..
logout() ->
	client ! logout.

-spec send(string(), string()) -> any().
%% @doc Send a message to specified user.
%% If client is running, sends a request to pass a message to specified user to server.
%% @end
send(To, Msg) ->
	case whereis(client) of
		undefined -> 
			client_not_logged;
		_ -> 
			client ! {message_to, {To, Msg}}
	end.
-spec who() -> any().
%% @doc Request lists of logged users.
%% Also checks if client is running.
%% @end
who() ->
	case whereis(client) of
		undefined ->
			client_not_logged;
		_ -> 
			client ! {who}
	end.

-spec yell(string()) -> any().
%% @doc Send a message to every logged user.
%% Also checks if client is running.
%% @end
yell(Msg) ->
	case whereis(client) of
		undefined ->
			client_not_logged;
		_ -> 
			client ! {yell, {Msg}}
	end.	
