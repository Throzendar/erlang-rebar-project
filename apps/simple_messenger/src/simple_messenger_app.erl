%% @private
-module(simple_messenger_app).

-behaviour(application).

%% Application callbacks
%%-export([start/0]).
-export([start/2, stop/1]).

%% ===================================================================
%% Application callbacks
%% ===================================================================

%%start() ->
%%	error_logger:info_msg("Starting application...~n"),
%%	application:start(simple_messenger).

start(_StartType, _StartArgs) ->
    simple_messenger_sup:start_link().

stop(_State) ->
    ok.
