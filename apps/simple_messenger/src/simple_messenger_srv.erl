%% @doc
%% This module contains callbacks for gen_server and client API
%% @end
-module(simple_messenger_srv).
-behaviour(gen_server).
-include("server_config.hrl").

%% ------------------------------------------------------------------
%% API Function Exports
%% ------------------------------------------------------------------

-export([start_link/0, login/2, logout/1, sendMessage/3, getUsersList/1,
		 sendYell/2
		]).

%% ------------------------------------------------------------------
%% gen_server Function Exports
%% ------------------------------------------------------------------

-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
         terminate/2, code_change/3]).

%% ------------------------------------------------------------------
%% API Function Definitions
%% ------------------------------------------------------------------

%% @private
start_link() ->
	error_logger:info_msg("Starting simple_messenger_server...~n"),
    gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

login(ServerNode, UserName) ->
	try gen_server:call({?SERVER, ServerNode}, {login, {UserName}}, 5000) of
		userName_already_in_use ->
			io:format("Username is already in use. Please use different username~n"),
			exit(normal);
		logged ->
			io:format("Logged~n")
	catch
		exit:Reason ->
			io:format("Server not responding, ~p~n", [Reason])
	end.

logout(ServerNode) ->
	gen_server:cast({?SERVER, ServerNode}, {logout, {self()}}),
	exit(normal).

sendMessage(ServerNode, To, Msg) ->
	try gen_server:call({?SERVER, ServerNode}, {message_to, {To, Msg}}) of
		message_invalid_destination ->
			io:format("Invalid destination~n");
		message_sent ->
			ok
	catch
		exit:_ ->
			io:format("Server is not responding...~n"),
			exit(timeout)
	end.

getUsersList(ServerNode) ->
	try gen_server:call({?SERVER, ServerNode}, {who}) of
		UsersList ->
			io:format("Users list: ~p~n", [UsersList])
	catch
		exit:_ ->
			io:format("Server is not responding...~n"),
			exit(timeout)
	end.

sendYell(ServerNode, Msg) ->
	try gen_server:call({?SERVER, ServerNode}, {yell, {Msg}}) of
		yell_sent ->
			ok
	catch
		exit:_ ->
			io:format("Server is not responding...~n"),
			exit(timeout)
	end.

%% ------------------------------------------------------------------
%% gen_server Function Definitions
%% ------------------------------------------------------------------

%% @private
init([]) ->
	error_logger:info_msg("Initializing with state []...~n"),
    {ok, []}.

%% @private
handle_call({login, {UserName}}, _From, State) ->
	%% error_logger:info_msg("~p, ~p wants to loggin~n",[_From, UserName]),
	case lists:keymember(UserName, 2, State) of
		true -> 
			%% error_logger:info_msg("State: ~p~n", [State]),
			{reply, userName_already_in_use, State};
		false -> 
			{Pid, _} = _From,
			NewState = [{Pid, UserName} | State],
			monitor(process, Pid),
			error_logger:info_msg("State: ~p~n", [NewState]),
			{reply, logged, NewState}
	end;

handle_call({message_to, {To, Msg}}, _From, State) ->
	%% error_logger:info_msg("~p sending message to ~p,~n '~p'~n",[_From, To, Msg]),
	{SenderPid, _} = _From,
	case lists:keysearch(SenderPid, 1, State) of
		fasle ->
			{noreply, State};
		{value, {_, To}} ->
			{reply, message_invalid_destination, State};
		{value, {_, SenderName}} ->
			case lists:keysearch(To, 2, State) of
				false -> 
					{reply, message_invalid_destination, State};
				{value, {Pid, _}} ->
					Pid ! {message_from, {SenderName, Msg}},
					{reply, message_sent, State}
			end
	end;

handle_call({who}, _From, State) ->
	Reply = lists:map(fun({_, X}) -> X end, State),
	error_logger:info_msg("~p~n", [Reply]),
	{reply, Reply, State};

handle_call({yell, {Msg}}, _From, State) ->
	{reply, yell_sent, State}.
%	PidList = lists:map(fun({X, _}) -> X end, State),
%	{SenderPid, _} = _From,
%	case lists:keysearch(SenderPid, 1, State) of
%		false ->
%			{noreply, State};
%		{value, {_, SenderName}} ->
%			lists:foreach(fun(Pid) -> Pid ! {yell_from, {SenderName, Msg}} end, PidList),
%			{reply, yell_sent, State}
%	end.


%% @private
handle_cast({logout, {Pid}}, State) ->
	%% error_logger:info_msg("~p wants to logout", [Pid]),
	NewState = logoutUser(Pid, State),
	{noreply, NewState}.


%% @private
handle_info({'DOWN', _MonitorRef, _, Pid, _}, State) ->
	error_logger:info_msg("Lost connection with ~p", [Pid]),
	NewState = logoutUser(Pid, State),
	{noreply, NewState};

handle_info(_Info, State) ->
    {noreply, State}.


%% @private
terminate(_Reason, _State) ->
    ok.


%% @private
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ------------------------------------------------------------------
%% Internal Function Definitions
%% ------------------------------------------------------------------

logoutUser(Pid, State) ->
	lists:keydelete(Pid, 1, State).
