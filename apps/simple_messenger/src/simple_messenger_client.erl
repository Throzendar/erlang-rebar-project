%% @doc
%% This module handles user requests and server replies. For more informations visit srv module page lined belowe.
%% @see simple_messenger_srv
%% @end
-module(simple_messenger_client).
-include("server_config.hrl").

%%% API
-export([loginClient/2]).

-spec loginClient(string(), string()) -> any().
%% @doc Send a login request to server.
loginClient(ServerNode, UserName) ->
	simple_messenger_srv:login(ServerNode, UserName),
	loop(ServerNode).

loop(ServerNode) ->
	receive
		%% Messages from client-side
		logout ->
			simple_messenger_srv:logout(ServerNode);
		{message_to, {To, Msg}} ->
			simple_messenger_srv:sendMessage(ServerNode, To, Msg),
			loop(ServerNode);
		{who} ->
			simple_messenger_srv:getUsersList(ServerNode),
			loop(ServerNode);
		{yell, {Msg}} ->
			simple_messenger_srv:sendYell(ServerNode, Msg),
			loop(ServerNode);
		
		%% Replies from server
		{message_from, {From, Msg}} ->
			io:format(">>>~p: ~p~n", [From, Msg]),
			loop(ServerNode);
		{yell_from, {From, Msg}} ->
			io:format(">>>~p shouted: ~p~n", [From, Msg]),
			loop(ServerNode)
	end.
